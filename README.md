# README #

## Author: Kayla Walker, kwalker3@uoregon.edu ##

Repository: bitbucket.org/kwalker3/kwalker3-proj1-pageserver

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

The objectives of this mini-project are:

  * Extend a tiny web server in Python, to check understanding of basic web architecture

### Usage ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

* (a) If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), send content of `name.html` or `name.css` with proper http response. (b) If `name.html` is not in current directory Respond with 404 (not found). (c) If a page starts with one of the symbols(~ // ..), respond with 403 forbidden error. For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.
  
  
